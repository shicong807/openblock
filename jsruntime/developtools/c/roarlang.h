
#ifndef ROARLANG_H
#define ROARLANG_H
typedef size_t FSM_PTR;
typedef size_t STATE_PTR;
typedef size_t MSG_PTR;
typedef size_t MSGHDLR_PTR;
typedef size_t DequeNode_PTR;
typedef size_t STR_PTR;
typedef size_t NOBJ_PTR;
typedef struct OB_FSM OB_FSM;
typedef struct OB_STATE OB_STATE;

#define OB_Message_Type_USERMSG 0
#define OB_Message_Type_EVENT 1
union OB_Message_Arg{
	size_t ptrdiff;
	double number;
	int integer;
};
typedef union OB_Message_Arg OB_Message_Arg;
typedef struct OB_Message
{
	char* name;
	char* argType;
	OB_Message_Arg arg;
	int type;
	FSM_PTR sender;
	/** 是否共享，用于广播，获取参数时需要复制 */
	char share;
} OB_Message;

typedef struct OB_Message OB_Message;
typedef struct OB_Deque_Node OB_Deque_Node;
typedef struct OB_VM OB_VM;
typedef void (*OB_OBJ_destroy)(OB_VM* ,void* ,size_t );
static void* __OB_malloc(OB_VM* vm, size_t size, char* typename,OB_OBJ_destroy destroy);
#define OB_malloc(vm,TYPE,READABLE_TYPE_NAME,destroy) __OB_malloc(vm,sizeof(TYPE),READABLE_TYPE_NAME,destroy)
static inline void* GetOBNObj(OB_VM* vm, size_t ob_ptr);
static inline size_t GetOBPtrByNative(OB_VM* vm, void* o);
typedef union {
	size_t ob_ptr;
	int64_t i64;
	double f64;
}OB_List_Item;
typedef struct{
	unsigned int length;
	unsigned int capacity;
	char link;
    char readonly;
	size_t items_ptr;
}OB_List;
static inline STR_PTR str2ptr(OB_VM* vm,char* str);
static inline void OB_link_obj(OB_VM* vm, size_t from,size_t memberOffset, size_t target);
static void OB_Message_destroy(OB_VM* vm , OB_Message* o,size_t ptr);
static size_t OB_New_List(OB_VM* vm , char link,char* obtypename);
static unsigned int OB_sizeOfMap(OB_VM* vm, size_t list_ptr);
static void OB_Insert_Value_At_Index(OB_VM* vm, size_t list_ptr,size_t index,size_t ptr);
static size_t OB_Get_Value_At_Index(OB_VM* vm, size_t list_ptr,size_t index);

#define GetNobjIntegerOrNumberField(type,fieldName,ptr) (((type *)(ptr))->fieldName)
#define GetNobjStringField(type,fieldName,ptr) (str2ptr(vm,((type *)(ptr))->fieldName))
int startOpenBlockVM();
int updateOpenBlockVM();
#endif